# mibs32

#### 介绍
为XX所所定制的mib文件

#### 运行
1 启动SNMPD
2 启动CXTABLE  (矢量subagent)
3 启动COLORNIX (标量subagent)
4 将矢量数据文件拷贝到 /data/for/目录下
mkdir -p /data/for
cp table/* /data/for/

#### 工作原理

1.  标量代码生成见src/generate.sh
2.  矢量代码生成见table/generate.sh
3.  代码不能使用C++代码，否则subagent无法自动加载
4.  表格定义mib必须使用Table和Entry，长度使用_len，这是约定处理。否则无法正常编译C，mibbrowser无法正常读数。

