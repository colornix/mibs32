/*
 * Note: this file originally auto-generated by mib2c
 * using mib2c.scalar.conf
 */
#ifndef NS_H
#define NS_H

/* function declarations */
void init_ns(void);
Netsnmp_Node_Handler handle_fwConnThresh;
Netsnmp_Node_Handler handle_fwConnNumber;
Netsnmp_Node_Handler handle_fwConnStaticNum;
Netsnmp_Node_Handler handle_fwConnDynamicNum;
Netsnmp_Node_Handler handle_fwConnDirect;
Netsnmp_Node_Handler handle_fwConnLower;
Netsnmp_Node_Handler handle_fwConnUpper;
Netsnmp_Node_Handler handle_fwConnType;
Netsnmp_Node_Handler handle_fwConnSearchIP;
Netsnmp_Node_Handler handle_fwConnTopSrcCount;
Netsnmp_Node_Handler handle_fwConnTopDesCount;
Netsnmp_Node_Handler handle_fwConnDetailCount;
Netsnmp_Node_Handler handle_fwHANodeNum;
Netsnmp_Node_Handler handle_fwHAThreshold;
Netsnmp_Node_Handler handle_fwHAFailedNum;
Netsnmp_Node_Handler handle_fwHAIPNum;
Netsnmp_Node_Handler handle_fwNetflowTimetype;
Netsnmp_Node_Handler handle_fwNetflowTimevalue;
Netsnmp_Node_Handler handle_fwNetflowSpecificIpvalue;
Netsnmp_Node_Handler handle_fwNetflowUrlid;
Netsnmp_Node_Handler handle_fwNetflowIfCount;
Netsnmp_Node_Handler handle_fwNetflowIpCount;
Netsnmp_Node_Handler handle_fwNetflowAppCount;
Netsnmp_Node_Handler handle_fwNetflowGroupCount;
Netsnmp_Node_Handler handle_fwNetflowUrlCount;
Netsnmp_Node_Handler handle_fwSecPolicyNum;
Netsnmp_Node_Handler handle_fwBwPolicyNum;
Netsnmp_Node_Handler handle_fwIpsPolicyNum;
Netsnmp_Node_Handler handle_fwAvPolicyNum;
Netsnmp_Node_Handler handle_fwApcPolicyNum;
Netsnmp_Node_Handler handle_fwUrlPolicyNum;
Netsnmp_Node_Handler handle_fwUpgradeServer;
Netsnmp_Node_Handler handle_fwUpgradePort;
Netsnmp_Node_Handler handle_fwUpgradeUser;
Netsnmp_Node_Handler handle_fwUpgradePass;
Netsnmp_Node_Handler handle_fwUpgradePacks;
Netsnmp_Node_Handler handle_fwUpgradeRetry;
Netsnmp_Node_Handler handle_fwUpgradeAct;
Netsnmp_Node_Handler handle_fwUpgradeState;
Netsnmp_Node_Handler handle_fwUpgradeDesc;
Netsnmp_Node_Handler handle_fwUpgradeReboot;
Netsnmp_Node_Handler handle_fwUpgradeHistoryNum;

#endif /* NS_H */
