env MIBS="+/usr/local/share/snmp/mibs/colornix-resource.mib" mib2c basemgt
env MIBS="+/usr/local/share/snmp/mibs/base-cfg.mib" mib2c basecfg
env MIBS="+/usr/local/share/snmp/mibs/colornix-lb.mib" mib2c lb
env MIBS="+/usr/local/share/snmp/mibs/colornix-ns.mib" mib2c ns
env MIBS="+/usr/local/share/snmp/mibs/colornix-ds.mib" mib2c ds
env MIBS="+/share/snmp/mibs/sysinfo.mib" mib2c SystemInformation

net-snmp-config --compile-subagent colornix --ldflags *.c
./colornix -Dswitch -f &

