

COCONET-CONFIG DEFINITIONS ::= BEGIN



IMPORTS
    MODULE-IDENTITY, OBJECT-TYPE, Integer32,
    IpAddress                                      FROM SNMPv2-SMI
    security                                    FROM COCONET-SMI;

config MODULE-IDENTITY
    LAST-UPDATED "9811240100Z"
    ORGANIZATION "COCONET Corporation"
    CONTACT-INFO
         "COCONET Corporation
          Beijing China
		 "

    DESCRIPTION
            "global config information."
    ::= { security 9 }

--
-- Domain server address table
--


enDomainSerTable OBJECT-TYPE
    SYNTAX      SEQUENCE OF EnDomainSerEntry
    MAX-ACCESS  not-accessible
    STATUS      current
    DESCRIPTION
            "Interface enumber"
    ::= { config 1 }

enDomainSerEntry OBJECT-TYPE
    SYNTAX      EnDomainSerEntry
    MAX-ACCESS  not-accessible
    STATUS      current
    DESCRIPTION
            "control information of the interface."
    INDEX   { enDomainSerIndex }
    ::= { enDomainSerTable 1 }

EnDomainSerEntry ::=
    SEQUENCE {
        enDomainSerIndex           Integer32,
        enDomainSerAddr            IpAddress
    }

enDomainSerIndex OBJECT-TYPE
    SYNTAX      Integer32 (0..2147483647)
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "domain server entry, should be start from 1 "
    ::= { enDomainSerEntry 1 }


enDomainSerAddr OBJECT-TYPE
    SYNTAX      IpAddress
    MAX-ACCESS  read-write
    STATUS      current
    DESCRIPTION
            "domain server IP address."
    ::= { enDomainSerEntry 2}


--
-- Log server address table
--

enLogSerTable OBJECT-TYPE
    SYNTAX      SEQUENCE OF EnLogSerEntry
    MAX-ACCESS  not-accessible
    STATUS      current
    DESCRIPTION
            "Log server entry."
    ::= { config 2 }

enLogSerEntry OBJECT-TYPE
    SYNTAX      EnLogSerEntry
    MAX-ACCESS  not-accessible
    STATUS      current
    DESCRIPTION
            "Log server index."
    INDEX   { enLogSerIndex }
    ::= { enLogSerTable 1 }

EnLogSerEntry ::=
    SEQUENCE {
        enLogSerIndex           Integer32,
        enLogSerAddr            IpAddress
    }

enLogSerIndex OBJECT-TYPE
    SYNTAX      Integer32 (0..2147483647)
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "A unique value, greater than zero, for each domain server 
            address.  It is recommended that values are assigned 
            contiguously starting from 1. "
    ::= { enLogSerEntry 1 }


enLogSerAddr OBJECT-TYPE
    SYNTAX      IpAddress
    MAX-ACCESS  read-write
    STATUS      current
    DESCRIPTION
            "The IP address to which this entry's addressing information
             pertains."
    ::= { enLogSerEntry 2}


--
-- Management server address table
--

enMngSerTable OBJECT-TYPE
    SYNTAX      SEQUENCE OF EnMngSerEntry
    MAX-ACCESS  not-accessible
    STATUS      current
    DESCRIPTION
            "A list of interface entries.  The number of entries is
            given by the value of ifPhyNumber."
    ::= { config 3 }

enMngSerEntry OBJECT-TYPE
    SYNTAX      EnMngSerEntry
    MAX-ACCESS  not-accessible
    STATUS      current
    DESCRIPTION
            "An entry containing management information applicable to a
            particular interface."
    INDEX   { enMngSerIndex }
    ::= { enMngSerTable 1 }

EnMngSerEntry ::=
    SEQUENCE {
        enMngSerIndex           Integer32,
        enMngSerAddr         IpAddress
    }

enMngSerIndex OBJECT-TYPE
    SYNTAX      Integer32 (0..2147483647)
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
            "A unique value, greater than zero, for each domain server 
            address.  It is recommended that values are assigned 
            contiguously starting from 1. "
    ::= { enMngSerEntry 1 }


enMngSerAddr OBJECT-TYPE
    SYNTAX      IpAddress
    MAX-ACCESS  read-write
    STATUS      current
    DESCRIPTION
            "The IP address to which this entry's addressing information
             pertains."
    ::= { enMngSerEntry 2}
    

enTimeSerAddr OBJECT-TYPE
    SYNTAX      IpAddress
    MAX-ACCESS  read-write
    STATUS      current
    DESCRIPTION
            "the time synchronizing server IP Address."
    ::= { config 4 }

END




