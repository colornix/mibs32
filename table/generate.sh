env MIBS="+/usr/local/share/snmp/mibs/colornix-lb.mib" mib2c -c /root/net-snmp-5.9.4/local/mib2c.iterate.conf lb
env MIBS="+/usr/local/share/snmp/mibs/colornix-ns.mib" mib2c -c /root/net-snmp-5.9.4/local/mib2c.iterate.conf ns
env MIBS="+/usr/local/share/snmp/mibs/colornix-ds.mib" mib2c -c /root/net-snmp-5.9.4/local/mib2c.iterate.conf ds
net-snmp-config --compile-subagent cxtable --ldflags *.c 
./cxtable -Dswitch -f &
